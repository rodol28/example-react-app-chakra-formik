export const users = [
    {
      id: 1,
      name: "User 1",
      email: "user1_email@example.com",
    },
    {
      id: 2,
      name: "User 2",
      email: "user2_email@example.com",
    },
    {
      id: 3,
      name: "User 3",
      email: "user3_email@example.com",
    },
    {
      id: 4,
      name: "User 4",
      email: "user4_email@example.com",
    },
  ];