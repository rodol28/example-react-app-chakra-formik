import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { UsersListPage, AddUsersPage } from "pages/backoffice/users";

export default function index() {
  return (
    <Router>
      <Switch>
        <Route exact path="/backoffice/users" component={UsersListPage} />
        <Route exact path="/backoffice/users/create" component={AddUsersPage} />
        <Redirect to="/backoffice/users" />
      </Switch>
    </Router>
  );
}
