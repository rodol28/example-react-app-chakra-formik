import React from "react";
import { Table, Thead, Tbody, Tr, Th } from "@chakra-ui/react";

import UsersTableRow from "components/backoffice/users/UsersTableRow";

export default function UsersTable({ users }) {
  return (
    <Table size="md">
      <Thead>
        <Tr>
          <Th>Nombre</Th>
          <Th>Email</Th>
          <Th>Opciones</Th>
        </Tr>
      </Thead>
      <Tbody>
        {users.map(user => {
          return <UsersTableRow user={user} key={user.id}/>;
        })}
      </Tbody>
    </Table>
  );
}
