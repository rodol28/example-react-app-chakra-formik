import React from "react";
import { Button } from "@chakra-ui/react";
import { EditIcon, DeleteIcon } from "@chakra-ui/icons";
import { colorSchema } from "const";

export default function UsersTableButton({ type }) {
  let bg = type === "edit" ? colorSchema.yellow : colorSchema.red;

  return (
    <Button bg={bg} size="sm" mx={"1rem"} paddingX={"1.5rem"} >
      {type === "edit" ? <EditIcon /> : <DeleteIcon />}
    </Button>
  );
}
