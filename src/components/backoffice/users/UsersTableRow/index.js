import React from "react";
import { Tr, Td } from "@chakra-ui/react";
import UsersTableButton from "components/backoffice/users/UsersTableButton";

export default function UserTableRow({ user }) {
  return (
    <Tr>
      <Td>{user.name}</Td>
      <Td>{user.email}</Td>
      <Td>
        <UsersTableButton type={'edit'}/>
        <UsersTableButton type={'delete'}/>
      </Td>
    </Tr>
  );
}
