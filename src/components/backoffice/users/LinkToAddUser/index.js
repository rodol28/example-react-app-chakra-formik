import React from "react";
import { AddIcon } from "@chakra-ui/icons";
import { Link } from "react-router-dom";
import styles from "./LinkToAddUser.module.css";

export default function UsersTableButton() {
  return (
    <Link to="/backoffice/users/create" className={styles.linkToAddUser}>
      Agregar usuario <AddIcon ml={"0.5rem"} />
    </Link>
  );
}