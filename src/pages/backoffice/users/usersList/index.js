import React from "react";
import { Container, Box, Center } from "@chakra-ui/react";

import { users } from "dataTest/users";
import { UsersTable, LinkToAddUser } from "components/backoffice/users";

export default function UsersListPage() {
  return (
    <Container maxW="container.md" mt={"10rem"}>
      <Center>
        <LinkToAddUser />
      </Center>
      <Box w="100%" color="black" mt={"2rem"}>
        <UsersTable users={users} />
      </Box>
    </Container>
  );
}